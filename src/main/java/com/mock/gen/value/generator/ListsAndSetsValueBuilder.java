package com.mock.gen.value.generator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.springframework.util.CollectionUtils;

public class ListsAndSetsValueBuilder implements ValueBuilder {

    private static final String LIST = "List";
    private static final String COLLECTION = "Collection";
    private static final String OBJECT = "Object";
    private final String typeName;
    private final ClassOrInterfaceType type;
    private VariableNameRepository variableNameRepository;
    private String expressionName;
    private ValueBuilderFactory valueBuilderFactory;
    private String collectionImplementation;

    public ListsAndSetsValueBuilder(ClassOrInterfaceType type, VariableNameRepository variableNameRepository,
            String expressionName, ValueBuilderFactory valueBuilderFactory) {
        this.type = type;
        this.variableNameRepository = variableNameRepository;
        this.expressionName = expressionName;
        this.valueBuilderFactory = valueBuilderFactory;
        typeName = this.type.getName().asString();

        if (typeName.equals("Set")) {
            collectionImplementation = "LinkedHashSet";
        } else {
            collectionImplementation = typeName;
        }
    }

    @Override
    public Result build() {
        return getResultLists();
    }

    private Result getResultLists() {

        List<Type> typeArgs = getTypeArgs();

        if (CollectionUtils.isEmpty(typeArgs)) {
            typeArgs = Arrays.asList(JavaParser.parseType(OBJECT));
        }

        Type listArg = typeArgs.get(0);
        ValueBuilder innerGenerator = valueBuilderFactory.get(listArg, null, variableNameRepository);
        Result innerResult = innerGenerator.build();

        String listArgName = listArg.toString();
        String expressionName = variableNameRepository.getExpressionName(this.expressionName, listArgName, typeName);

        boolean useAsList = typeName.equals(LIST) || typeName.equals(COLLECTION);

        String listConstruction = !useAsList ? "new " + collectionImplementation + "<>()"
                : "Arrays.asList(" + innerResult.getExpression() + ")";

        String statement =
                typeName + "<" + listArgName + "> " + expressionName + " = " + listConstruction + ";" + System
                        .lineSeparator();
        if (!useAsList) {
            statement += expressionName + ".add(" + innerResult.getExpression() + ");";
        }

        Collection<Statement> statements = new LinkedList<>();
        statements.addAll(innerResult.getStatements());
        statements.addAll(JavaParser.parseBlock("{" + statement + "}").getStatements());
        Expression expression = JavaParser.parseExpression(expressionName);
        return new Result(expression, statements);
    }

    private List<Type> getTypeArgs() {
        return type.getTypeArguments().isPresent() ? type.getTypeArguments().get() : Arrays.asList();
    }
}

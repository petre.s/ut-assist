package com.mock.gen.value.generator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.util.CollectionUtils;


public class MapsValueBuilder implements ValueBuilder {

    private static final String OBJECT = "Object";
    private static final String LINKED_HASH_MAP = "LinkedHashMap";
    private final ClassOrInterfaceType type;
    private String collectionImplementation;
    private final String typeName;
    private String expressionName;
    private VariableNameRepository variableNameRepository;
    private ValueBuilderFactory valueBuilderFactory;

    public MapsValueBuilder(ClassOrInterfaceType type, String expressionName,
            VariableNameRepository variableNameRepository,
            ValueBuilderFactory valueBuilderFactory) {
        this.type = type;
        this.expressionName = expressionName;
        this.variableNameRepository = variableNameRepository;
        this.valueBuilderFactory = valueBuilderFactory;
        typeName = this.type.getName().asString();
        if (typeName.equals("Map")) {
            collectionImplementation = LINKED_HASH_MAP;
        } else {
            collectionImplementation = typeName;
        }
    }

    public Result build() {
        Collection<Statement> statements = new LinkedList<>();

        List<Type> typeArgs = getTypeArgs();

        if (CollectionUtils.isEmpty(typeArgs)) {
            typeArgs = Arrays.asList(JavaParser.parseType(OBJECT), JavaParser.parseType(OBJECT));
        }

        ValueBuilder keyGenerator = valueBuilderFactory.get(typeArgs.get(0), null, variableNameRepository);
        Result keyResult = keyGenerator.build();

        ValueBuilder valueGenerator = valueBuilderFactory.get(typeArgs.get(1), null, variableNameRepository);
        Result valueResult = valueGenerator.build();

        String valueArgType = typeArgs.get(1).toString();
        String variableName = variableNameRepository.getExpressionName(expressionName, valueArgType, typeName);

        String listConstruction = "new " + collectionImplementation + "<>()";

        String statement =
                typeName + "<" + typeArgs.stream().map(t -> t.toString()).collect(Collectors.joining(",")) + "> "
                        + variableName + " = " + listConstruction + ";" + System.lineSeparator();

        statement += variableName + ".put(" + keyResult.getExpression() + ", " + valueResult.getExpression() + ");";

        statements.addAll(keyResult.getStatements());
        statements.addAll(valueResult.getStatements());
        statements.addAll(JavaParser.parseBlock("{" + statement + "}").getStatements());
        Expression expression = JavaParser.parseExpression(variableName);
        return new Result(expression, statements);
    }

    private List<Type> getTypeArgs() {
        return type.getTypeArguments().isPresent() ? type.getTypeArguments().get() : Arrays.asList();
    }

}

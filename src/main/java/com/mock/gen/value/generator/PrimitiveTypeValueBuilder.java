package com.mock.gen.value.generator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import java.util.Collections;
import java.util.Random;

public class PrimitiveTypeValueBuilder implements ValueBuilder {

    private static final int MAX_GENERATED_NO = 10;
    private static final int RANDOM_LIMIT = MAX_GENERATED_NO + 1;
    private static final String INTEGER = "Integer";
    private static final String LONG = "Long";
    private static final String SHORT = "Short";
    private static final String BOOLEAN = "Boolean";
    private static final String BYTE = "Byte";
    private static final String DOUBLE = "Double";
    private static final String FLOAT = "Float";
    private final ClassOrInterfaceType type;
    private final String typeName;

    public PrimitiveTypeValueBuilder(ClassOrInterfaceType type) {
        this.type = type;
        typeName = this.type.getName().asString();
    }

    @Override
    public Result build() {

        Random rand = new Random();
        if (typeName.equals(INTEGER) || typeName.equals(LONG) || typeName.equals(SHORT) || typeName
                .equals(BYTE)) {
            return new Result(JavaParser.parseExpression(String.valueOf(rand.nextInt(RANDOM_LIMIT))),
                    Collections.emptyList());
        }

        if (typeName.equals(BOOLEAN)) {

            return new Result(JavaParser.parseExpression(String.valueOf(rand.nextBoolean())), Collections.emptyList());
        }

        if (typeName.equals(DOUBLE) || typeName.equals(FLOAT)) {
            return new Result(
                    JavaParser.parseExpression(String.valueOf(rand.nextFloat() + rand.nextInt(RANDOM_LIMIT - 1))),
                    Collections.emptyList());
        }

        throw new IllegalArgumentException();
    }
}

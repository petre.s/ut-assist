package com.mock.gen.value.generator;

import java.util.HashMap;
import java.util.Map;

public class VariableNameRepository {

    private Map<String, Integer> usedIdentifiers;

    public VariableNameRepository() {
        this.usedIdentifiers = new HashMap<>();
    }

    public String getExpressionName(String desiredName, String typeName) {
        return getExpressionName(desiredName, typeName, null);
    }

    public String getExpressionName(String desiredName, String typeName, String suffix) {
        if (desiredName == null && typeName == null) {
            throw new IllegalArgumentException("Either desiredName or typeName must be not null");
        }

        String expressionName = (desiredName != null ? desiredName
                : typeName.substring(0, 1).toLowerCase() + typeName.substring(1))
                + (suffix != null ? suffix : "");

        Integer count = usedIdentifiers.get(expressionName);
        if (count == null) {
            usedIdentifiers.put(expressionName, 0);
        } else {
            count++;
            usedIdentifiers.put(expressionName, count);
            expressionName = expressionName + count;
        }
        return expressionName;
    }
}

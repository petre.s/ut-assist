package com.mock.gen.value.generator;

import com.github.javaparser.ast.type.Type;

public interface ValueBuilderFactory {

    ValueBuilder get(Type type, String expressionName, VariableNameRepository variableNameRepository);
}

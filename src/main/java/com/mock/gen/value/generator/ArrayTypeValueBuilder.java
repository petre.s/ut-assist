package com.mock.gen.value.generator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.type.ArrayType;
import com.github.javaparser.ast.type.Type;
import java.util.Collection;
import java.util.LinkedList;

public class ArrayTypeValueBuilder implements ValueBuilder {

    ArrayType type;
    private String expressionName;
    private VariableNameRepository variableNameRepository;
    private ValueBuilderFactory valueBuilderFactory;

    public ArrayTypeValueBuilder(ArrayType type, String expressionName, VariableNameRepository variableNameRepository,
            ValueBuilderFactory valueBuilderFactory) {
        this.type = type;
        this.expressionName = expressionName;
        this.variableNameRepository = variableNameRepository;
        this.valueBuilderFactory = valueBuilderFactory;
    }

    @Override
    public Result build() {
        Type elementType = type.getComponentType();
        ValueBuilder innerGenerator = valueBuilderFactory.get(elementType, null, variableNameRepository);
        Result innerResult = innerGenerator.build();

        String arrayConstruction = "new " + elementType + "[]{" + innerResult.getExpression() + "}";
        Collection<Statement> statements = new LinkedList<>();

        Expression expression;
        if (expressionName != null) {
            String statement =
                    elementType + "[] " + expressionName + " = " + arrayConstruction + ";" + System.lineSeparator();
            statements.addAll(JavaParser.parseBlock("{" + statement + "}").getStatements());
            expression = JavaParser.parseExpression(expressionName);
            ;
        } else {
            expression = JavaParser.parseExpression(arrayConstruction);
        }

        statements.addAll(innerResult.getStatements());
        return new Result(expression, statements);
    }
}

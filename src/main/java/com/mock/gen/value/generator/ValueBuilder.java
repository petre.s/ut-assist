package com.mock.gen.value.generator;

import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.Statement;
import java.util.Collection;

public interface ValueBuilder {

    Result build();

    class Result {

        private Expression expression;
        private Collection<Statement> statements;

        public Result(Expression expression, Collection<Statement> statements) {
            this.expression = expression;
            this.statements = statements;
        }

        public Expression getExpression() {
            return expression;
        }

        public Collection<Statement> getStatements() {
            return statements;
        }
    }
}

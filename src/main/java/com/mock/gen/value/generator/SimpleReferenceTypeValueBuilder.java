package com.mock.gen.value.generator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import java.util.Collections;
import java.util.Random;

public class SimpleReferenceTypeValueBuilder implements ValueBuilder {

    private static final String STRING = "String";
    private static final String OBJECT = "Object";
    private final String typeName;
    private ClassOrInterfaceType type;
    private String expressionName;
    private VariableNameRepository variableNameRepository;

    String[] randomStrings = new String[]{"purpose", "city", "host", "face", "tile", "sport", "mislead", "coincide",
            "useful", "commitment", "mechanical", "summit", "musical", "draft", "identification", "study", "cell",
            "basketball", "regard", "addition", "corn", "break in", "endure", "highlight", "color", "passage",
            "parallel", "dangerous", "fire", "combine", "exit", "reality", "time", "reaction", "mosquito", "powder",
            "include", "need", "evolution", "flexible", "occupation", "volcano", "person", "refund", "push",
            "dimension", "unlike", "care", "tie", "dynamic", "goat", "creep", "work out", "bless", "excuse", "minority",
            "touch", "sell", "snack", "tired", "mass", "workshop", "bad", "meeting", "integrated", "spider", "viable",
            "tower", "disappear", "arm", "grip", "correspondence", "fairy", "gasp", "hard", "Venus", "march",
            "interface", "allow", "hand", "foot", "right", "progress"};

    public SimpleReferenceTypeValueBuilder(ClassOrInterfaceType type, String expressionName,
            VariableNameRepository variableNameRepository) {

        this.type = type;
        this.expressionName = expressionName;
        this.variableNameRepository = variableNameRepository;
        this.typeName = this.type.getName().asString();
    }

    @Override
    public Result build() {
        if (typeName.equals(STRING)) {
            Random random = new Random();

            String randomString = randomStrings[random.nextInt(randomStrings.length)];
            return new Result(JavaParser.parseExpression("\"" + randomString + "\""), Collections.emptyList());
        }

        if (typeName.equals(OBJECT)) {
            return new Result(JavaParser.parseExpression("new Object()"), Collections.emptyList());
        }

        String expressionNameFinal = variableNameRepository.getExpressionName(expressionName, typeName);

        Expression expression = JavaParser.parseExpression(expressionNameFinal);

        Statement statement = JavaParser.parseBlock("{" +
                type.toString() + " " + expressionNameFinal + " =  mock(" + type.toString() + ".class); }");
        return new Result(expression, ((BlockStmt) statement).getStatements());
    }


}

package com.mock.gen.value.generator;

import com.github.javaparser.ast.type.ArrayType;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.PrimitiveType;
import com.github.javaparser.ast.type.PrimitiveType.Primitive;
import com.github.javaparser.ast.type.Type;
import java.util.stream.Stream;


public class ValueBuilderFactoryImpl implements ValueBuilderFactory {

    private static final String LIST = "List";
    private static final String COLLECTION = "Collection";
    private static final String ARRAY_LIST = "ArrayList";
    private static final String SET = "Set";
    private static final String HASH_SET = "HashSet";
    private static final String LINKED_LIST = "LinkedList";
    private static final String MAP = "Map";
    private static final String TREE_MAP = "TreeMap";
    private static final String HASH_MAP = "HashMap";
    private static final String LINKED_HASH_MAP = "LinkedHashMap";

    @Override
    public ValueBuilder get(Type type, String expressionName, VariableNameRepository variableNameRepository) {
        if (isPrimitive(type)) {
            return new PrimitiveTypeValueBuilder(getPrimitiveBoxedType(type));
        } else if (isArray(type)) {
            return new ArrayTypeValueBuilder((ArrayType) type, expressionName, variableNameRepository, this);
        } else if (isMap(type)) {
            return new MapsValueBuilder((ClassOrInterfaceType) type, expressionName, variableNameRepository, this);
        } else if (isListOrSet(type)) {
            return new ListsAndSetsValueBuilder((ClassOrInterfaceType) type, variableNameRepository, expressionName,
                    this);
        } else {
            return new SimpleReferenceTypeValueBuilder((ClassOrInterfaceType) type, expressionName,
                    variableNameRepository);
        }
    }

    private boolean isPrimitive(Type type) {
        return getPrimitiveBoxedType(type) != null;
    }

    private ClassOrInterfaceType getPrimitiveBoxedType(Type type) {
        if (type instanceof PrimitiveType) {
            return ((PrimitiveType) type).toBoxedType();
        }
        return Stream.of(Primitive.values()).filter(s -> s.toBoxedType().equals(type)).map(e -> e.toBoxedType())
                .findAny().orElse(null);
    }

    private boolean isArray(Type type) {
        return type instanceof ArrayType;
    }

    private boolean isListOrSet(Type type) {
        if (!(type instanceof ClassOrInterfaceType)) {
            return false;
        }

        String typeName = ((ClassOrInterfaceType) type).getName().asString();

        return typeName.equals(LIST) || typeName.equals(COLLECTION) || typeName.equals(ARRAY_LIST) || typeName
                .equals(LINKED_LIST) || typeName.equals(HASH_SET) || typeName.equals(SET);
    }

    private boolean isMap(Type type) {
        if (!(type instanceof ClassOrInterfaceType)) {
            return false;
        }

        String typeName = ((ClassOrInterfaceType) type).getName().asString();
        return typeName.equals(MAP) || typeName.equals(HASH_MAP) || typeName.equals(LINKED_HASH_MAP) || typeName
                .equals(TREE_MAP);

    }
}

package com.mock.gen;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.CallableDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.mock.gen.value.generator.ValueBuilder.Result;
import com.mock.gen.value.generator.ValueBuilderFactoryImpl;
import com.mock.gen.value.generator.VariableNameRepository;
import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Generator {

    private ClassOrInterfaceDeclaration clazz;
    private CompilationUnit compilationUnit;
    private String scopeName;
    private CallableDeclaration target;

    VariableNameRepository variableNameRepository = new VariableNameRepository();

    public Generator(String declaration, String clazz) {
        compilationUnit = JavaParser.parse(new ByteArrayInputStream(clazz.getBytes()));

        this.target = findTarget(declaration);

        if (this.target == null) {
            throw new RuntimeException();
        }

        this.clazz = target.findParent(ClassOrInterfaceDeclaration.class).get();
        composeScopeName();
    }

    private CallableDeclaration findTarget(String declaration) {
        declaration = declaration.trim();
        declaration += "{}";

        MethodVisitor methodVisitor = new MethodVisitor(
                (CallableDeclaration) JavaParser.parseBodyDeclaration(declaration));

        compilationUnit.accept(methodVisitor, null);
        return methodVisitor.getFound();
    }

    private void composeScopeName() {
        if (target.isStatic()) {
            scopeName = clazz.getNameAsString();
            return;
        }

        String firstLetter = clazz.getName().asString().substring(0, 1);
        scopeName = firstLetter.toLowerCase().equals(firstLetter) ? clazz.getName() + "Instance"
                : firstLetter.toLowerCase() + clazz.getName().asString().substring(1);
    }

    public String generate() {

        List<Statement> statements = new LinkedList<>();
        List<Expression> parameters = new LinkedList<>();

        List<Statement> constructorStatements = new LinkedList<>();
        List<Expression> constructorParameters = new LinkedList<>();

        String creation = target.isStatic() ? "" :
                getCreation(statements, parameters, constructorStatements, constructorParameters);

        String testMethodName = target.getName() + "Test";

        if (!(target instanceof ConstructorDeclaration)) {
            buildParameters(statements, parameters, target.getParameters());
        }

        String act = target instanceof ConstructorDeclaration ? ""
                : System.lineSeparator() + joinBy(statements, System.lineSeparator()) + System.lineSeparator()
                        + scopeName + "." + target
                        .getName() + "(" + joinBy(parameters, ", ") + ");" + System.lineSeparator();

        String result = " @Test"
                + System.lineSeparator()
                + "    public void " + testMethodName + "() {" + System.lineSeparator()
                + creation
                + act
                + "    }";

        result = JavaParser.parseBodyDeclaration(result).toString();
        System.out.println(result + "\n\n");
        return result;
    }

    private String getCreation(List<Statement> statements, List<Expression> parameters,
            List<Statement> constructorStatements, List<Expression> constructorParameters) {
        Optional<ConstructorDeclaration> constructor = getConstructor(target, clazz);

        constructor.ifPresent(constructorDeclaration -> buildParameters(constructorStatements, constructorParameters,
                constructorDeclaration.getParameters()));

        return joinBy(constructorStatements, System.lineSeparator()) + clazz.getName() + " " + scopeName + " = new "
                + clazz.getName() + "(" + joinBy(constructorParameters, ", ") + ");";
    }

    private <O extends Object> String joinBy(Collection<O> objects, CharSequence joinSeq) {
        return objects.stream().filter(Objects::nonNull).map(p -> p.toString()).collect(Collectors.joining(joinSeq));
    }

    private void buildParameters(List<Statement> statements, List<Expression> parameters,
            NodeList<Parameter> declaredParameters) {
        Objects.requireNonNull(statements);
        Objects.requireNonNull(parameters);
        Objects.requireNonNull(declaredParameters);
        ValueBuilderFactoryImpl factory = new ValueBuilderFactoryImpl();
        declaredParameters.forEach(parameter -> {
            Result valueGeneratorResult = factory
                    .get(parameter.getType(), parameter.getNameAsString(), variableNameRepository).build();
            statements.addAll(valueGeneratorResult.getStatements());
            parameters.add(valueGeneratorResult.getExpression());
        });
    }

    private Optional<ConstructorDeclaration> getConstructor(CallableDeclaration method,
            ClassOrInterfaceDeclaration clazz) {
        if (method instanceof ConstructorDeclaration) {
            return Optional.of((ConstructorDeclaration) method);
        }
        return clazz.findAll(ConstructorDeclaration.class).stream().findFirst();
    }

    private static class MethodVisitor extends VoidVisitorAdapter<Void> {

        private CallableDeclaration found;
        private CallableDeclaration whatToSearch;

        public MethodVisitor(CallableDeclaration whatToSearch) {
            this.whatToSearch = whatToSearch;
        }

        private boolean thisIsIt(CallableDeclaration methodDeclaration) {
            return parametersListCompare(methodDeclaration) && whatToSearch.getName()
                    .equals(methodDeclaration.getName());
        }

        private boolean parametersListCompare(CallableDeclaration methodDeclaration) {
            if (whatToSearch.getParameters().size() != methodDeclaration.getParameters().size()) {
                return false;
            }
            for (int i = 0; i < whatToSearch.getParameters().size(); i++) {
                if (!whatToSearch.getParameter(i).getType().equals(methodDeclaration.getParameter(i).getType())) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public void visit(ConstructorDeclaration n, Void arg) {
            if (check(n)) {
                super.visit(n, arg);
            }
        }

        @Override
        public void visit(final MethodDeclaration method, final Void arg) {
            if (check(method)) {
                super.visit(method, arg);
            }
        }

        private boolean check(CallableDeclaration n) {
            if (found != null) {
                return false;
            }

            if (thisIsIt(n)) {
                found = n;
                return false;
            }
            return true;
        }

        public CallableDeclaration getFound() {
            return found;
        }
    }
}

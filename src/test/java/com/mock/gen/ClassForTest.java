package com.mock.gen;

import org.junit.Test;
import org.springframework.lang.NonNull;

import java.io.File;
import java.util.*;

public class ClassForTest {

    public ClassForTest(int a){

    }

    public ClassForTest(boolean a){

    }

    public static void main(String[] arr) {

    }

    public static void main2(String[][][] arr) {

    }

    public static void myStaticMethod(List<Integer> listPar) {
    }

    public void method1(){
        int a=4;
    }

    public int method2(){
        return 3;
    }

    public int method3(int a){
        return a;
    }

    public int method3(int a, Integer b){
        return a;
    }

    public void methodListList(List<List<Integer>> lists) {

    }

    public void methodListListBam(List<Map<Set<Integer>, String>> integerSet) {

    }

    public void methodList(List<String> myList){

    }

    public void methodListFile(List<File> myList){

    }

    public void methodListRaw(List myList){

    }

    public void methodList(ArrayList<String> myList){

    }

    public void methodList(LinkedList<String> myList){

    }

    public void methodCollection(Collection<String> collection){

    }

    public void methodSet(Set<String> mySet){

    }

    public void methodSet(HashSet<String> mySet){

    }

    public void methodMap(Map<String, String> map){

    }

    public void methodMap(HashMap<String, String> map){

    }

    public void methodMap(LinkedHashMap<String, Float> map) {

    }

    @NonNull
    public void annotatedMethod( @NonNull LinkedHashMap<String, Float>  map) {

    }

    public void methodMock(Calendar calendar){

    }

    public void createTicket(File ticket, List<Ticket> ticketsList){}

    class Ticket{}
}

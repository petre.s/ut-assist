package com.mock.gen;

import static org.junit.Assert.assertFalse;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

public class GeneratorTest {
    String clazz = "package com.mock.gen;\n" +
            "\n" +
            "import org.junit.Test;\n" +
            "import org.springframework.lang.NonNull;\n" +
            "\n" +
            "import java.io.File;\n" +
            "import java.util.*;\n" +
            "\n" +
            "public class ClassForTest {\n" +
            "\n" +
            "    public ClassForTest(int a){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public ClassForTest(boolean a){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public static void main(String[] arr) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public static void main2(String[][][] arr) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public static void myStaticMethod(List<Integer> listPar) {\n" +
            "    }\n" +
            "\n" +
            "    public void method1(){\n" +
            "        int a=4;\n" +
            "    }\n" +
            "\n" +
            "    public int method2(){\n" +
            "        return 3;\n" +
            "    }\n" +
            "\n" +
            "    public int method3(int a){\n" +
            "        return a;\n" +
            "    }\n" +
            "\n" +
            "    public int method3(int a, Integer b){\n" +
            "        return a;\n" +
            "    }\n" +
            "\n" +
            "    public void methodListList(List<List<Integer>> lists) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodListListBam(List<Map<Set<Integer>, String>> integerSet) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodList(List<String> myList){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodListFile(List<File> myList){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodListRaw(List myList){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodList(ArrayList<String> myList){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodList(LinkedList<String> myList){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodCollection(Collection<String> collection){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodSet(Set<String> mySet){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodSet(HashSet<String> mySet){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodMap(Map<String, String> map){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodMap(HashMap<String, String> map){\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodMap(LinkedHashMap<String, Float> map) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    @NonNull\n" +
            "    public void annotatedMethod( @NonNull LinkedHashMap<String, Float>  map) {\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    public void methodMock(Calendar calendar){\n" +
            "\n" +
            "    }\n" +
            "    \n" +
            "    public void createTicket(File ticket, List<Ticket> ticketsList){}\n" +
            "    \n" +
            "    class Ticket{}\n" +
            "}\n";

    @Test
    public void generate() throws ParseException {


        CompilationUnit compilationUnit = JavaParser.parse(new ByteArrayInputStream(clazz.getBytes()));

        MethodVisitorTest methodVisitor = new MethodVisitorTest();
        compilationUnit.accept(methodVisitor, null);

        List<String> stringList = methodVisitor.getFound().stream()
                .map(m -> new Generator(m.getDeclarationAsString(), clazz).generate()).collect(Collectors.toList());

        assertFalse(stringList.isEmpty());
    }

    @Test
    public void test5(){
        Generator generator = new Generator("public ClassForTest(boolean a) ", clazz );
        generator.generate();
    }

    @Test
    public void test6() {
        Generator generator = new Generator("public static void myStaticMethod(List<Integer> listPar)", clazz);
        generator.generate();
    }

    @Test
    public void test7() {
        Generator generator = new Generator("public void methodListListBam(List<Map<Set<Integer>, String>> integerSet)",
                clazz);
        generator.generate();
    }

    @Test
    public void test8() {
        Generator generator = new Generator("public static void main(String [] arr)", clazz);
        generator.generate();
    }

    @Test
    public void test9() {
        Generator generator = new Generator("public static void main2(String [][][] arr)", clazz);
        generator.generate();
    }
    @Test
    public void test10() {
        Generator generator = new Generator(" @Test public void annotatedMethod( @NonNull LinkedHashMap<String, Float>  map)", clazz);
        String generate = generator.generate();
        Assert.assertTrue(generate.contains("LinkedHashMap"));

    }

    @Test
    public void test11() {
        Generator generator = new Generator("public void annotatedMethod( LinkedHashMap<String, Float>  map)", clazz);
        String generate = generator.generate();
        Assert.assertTrue(generate.contains("LinkedHashMap"));
    }

    @Test
    public void test12() {
        Generator generator = new Generator("public void methodMock(Calendar calendar)", clazz);
        String generate = generator.generate();
        Assert.assertTrue(generate.contains("Calendar"));
    }

    @Test
    public void test13() {
        Generator generator = new Generator("public void createTicket(File ticket, List<Ticket> ticketsList)", clazz);
        String generate = generator.generate();
        Assert.assertTrue(generate.contains("ticket"));
        Assert.assertTrue(generate.contains("ticket1"));
    }

    @Test
    public void testConstructorGen(){
        CompilationUnit compilationUnit = JavaParser.parse(new ByteArrayInputStream(clazz.getBytes()));
        Generator generator = new Generator(compilationUnit.findFirst(ConstructorDeclaration.class).get().getDeclarationAsString(), clazz);
        generator.generate();
    }

    class MethodVisitorTest extends VoidVisitorAdapter<Void> {

        private Collection<MethodDeclaration> found = new LinkedList<>();

        @Override
        public void visit(final MethodDeclaration method, final Void arg) {

            super.visit(method, arg);
            found.add(method);
        }

        public Collection<MethodDeclaration> getFound() {
            return found;
        }
    }
}